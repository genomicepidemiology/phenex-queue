#!/usr/bin/env python3

import os
import sys
import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(os.environ['RABBITMQ-HOST']))

EXCHANGE = 'web'
queue = sys.argv[1]
message = ' '.join(sys.argv[2:])

channel = connection.channel()
channel.queue_declare(queue, exclusive=False)
channel.exchange_declare(exchange=EXCHANGE, exchange_type='direct')
channel.basic_publish(exchange=EXCHANGE, routing_key=queue, body=message, properties=pika.BasicProperties(delivery_mode=2))
print(f"[x] Sent: '{message}' to '{queue}' queue with '{EXCHANGE}' exchange.")

connection.close()
