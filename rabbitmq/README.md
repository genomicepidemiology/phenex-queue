# Notes

This implementation is a combines solution of [working que](https://www.rabbitmq.com/tutorials/tutorial-two-python.html).

# Instruction

1. Open five terminal windows.
2. In **first** window run `docker-compose up -d`
5. In **second** window run `docker exec -t rabbit-consumer-one python3 /src/receive.py svc.resfinder.v1`
5. In **third** window run `docker exec -t rabbit-consumer-one python3 /src/receive.py svc.resfinder.v1`
6. In **fourth** window run `docker exec -t rabbit-consumer-two python3 /src/receive.py svc.mlst.v1`
4. In **fifth** window run `docker exec rabbit-broker rabbitmqctl list_queues`
4. In **fifth** window run `docker exec rabbit-broker rabbitmqctl list_bindings`
3. In **sixth** window run `docker exec rabbit-producer python3 /src/send.py svc.resfinder.v1 5`
3. In **sixth** window run `docker exec rabbit-producer python3 /src/send.py svc.resfinder.v1 6`
3. In **sixth** window run `docker exec rabbit-producer python3 /src/send.py svc.resfinder.v1 7`
3. In **sixth** window run `docker exec rabbit-producer python3 /src/send.py svc.resfinder.v1 8`
3. In **sixth** window run `docker exec rabbit-producer python3 /src/send.py svc.mlst.v1 5`
