#!/usr/bin/env python3

"""
This files is both receiver and producer.
It receives messages from 'web' exchange over the queue set in argv.
It produces messages over 'svc' exchange to 'svc.results' queue.
"""

import sys
import pika
import time

EXCHANGE = 'web'
queue = sys.argv[1:]
if not queue:
    sys.stderr.write("Exiting: missing queue name!\n")
    sys.exit(1)

queue_name = queue[0]

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker'))

channel = connection.channel()
channel.exchange_declare(exchange=EXCHANGE, exchange_type='direct')
channel.exchange_declare(exchange='svc', exchange_type='fanout')
channel.queue_declare(queue_name, exclusive=False)
channel.queue_declare('svc.results', exclusive=False)
channel.queue_bind(exchange=EXCHANGE, queue=queue_name)
channel.queue_bind(exchange='svc', queue='svc.results')

print('[*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(f"[x] {properties.correlation_id} Received {body}")
    delay = int(body)
    time.sleep(delay)
    print(f"[x] Done after {delay} seconds")
    ch.basic_ack(delivery_tag=method.delivery_tag)
    ch.basic_publish(exchange='svc', routing_key='svc.results', body=body, properties=pika.BasicProperties(
        delivery_mode=2,
        correlation_id=properties.correlation_id
    ))
    print(" [x] %r Sent %r" % (properties.correlation_id, body))


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=queue_name, auto_ack=False, on_message_callback=callback)

channel.start_consuming()
