#!/usr/bin/env python3

"""
It receives messages from 'svc' exchange over 'svc.results' queue.
"""

import sys
import pika
import time

EXCHANGE = 'svc'
QUEUE_NAME = 'svc.results'

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker'))

channel = connection.channel()
channel.exchange_declare(exchange=EXCHANGE, exchange_type='fanout')
channel.queue_declare(QUEUE_NAME, exclusive=False)
channel.queue_bind(exchange=EXCHANGE, queue=QUEUE_NAME)

print('[*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print(f"[x] {properties.correlation_id} Received {body}")
    delay = int(body)
    time.sleep(delay)
    print(f"[x] Done after {delay} seconds")
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_consume(queue=QUEUE_NAME, auto_ack=False, on_message_callback=callback)

channel.start_consuming()
