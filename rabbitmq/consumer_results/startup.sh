#!/usr/bin/env bash

pip3 install -r /src/requirements.txt

# sleep infinity
sleep 6

python3 /src/receive.py
