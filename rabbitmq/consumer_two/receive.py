#!/usr/bin/env python3

import sys
import pika
import time

EXCHANGE = 'web'
queue = sys.argv[1:]
if not queue:
    sys.stderr.write("Exiting: missing queue name!\n")
    sys.exit(1)

queue_name = queue[0]

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker'))

channel = connection.channel()
channel.exchange_declare(exchange=EXCHANGE, exchange_type='direct')
channel.queue_declare(queue_name, exclusive=False)
channel.queue_bind(exchange=EXCHANGE, queue=queue_name)

print('[*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    print("[x] Received %r" % body)
    delay = int(body)
    time.sleep(delay)
    print(f"[x] Done after {delay} seconds")
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=queue_name, auto_ack=False, on_message_callback=callback)

channel.start_consuming()
