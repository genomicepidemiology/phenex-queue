#!/usr/bin/env python3

import sys
import pika
import uuid

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbit-broker'))

EXCHANGE = 'web'
queue = sys.argv[1]
message = ' '.join(sys.argv[2:])

channel = connection.channel()
channel.queue_declare(queue, exclusive=False)
channel.exchange_declare(exchange=EXCHANGE, exchange_type='direct')
channel.basic_publish(exchange=EXCHANGE, routing_key=queue, body=message, properties=pika.BasicProperties(
    delivery_mode=2,
    correlation_id=str(uuid.uuid4())
))
print(f"[x] Sent: '{message}' to '{queue}' queue with '{EXCHANGE}' exchange.")

connection.close()
